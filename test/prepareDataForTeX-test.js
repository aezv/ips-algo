var mocha  = require('mocha')
  , assert = require('chai').assert
  , expect = require('chai').expect
  , PointSet = require('../PointSet.js')
  ;

describe("Testing prepareDataForTeX() function", function(){


    it("Should treat usual PS as without any problems", function(done){
        expect(
			new PointSet(
			1n,
			1n,
			[
				{ x:0n, y:      0n},
				{ x:0n, y:      3n},
				{ x:4n, y:      0n},
			])
			.prepareDataForTeX()
        ).to.eql(
			'\\sqrt{1}/1*\\{( 0 ; 0); \n( 0 ; 3); \n( 4 ; 0); \n\\}'
        );
        done();
    });

    it("Should squash symmetric points", function(done){
        expect(
			new PointSet(
			1n,
			1n,
			[
				{ x: 0n, y:      0n},
				{ x: 0n, y:     -3n},
				{ x: 4n, y:      0n},
				{ x: 0n, y:      3n},
				{ x:-4n, y:      0n},
			])
			.prepareDataForTeX()
        ).to.eql(
			'\\sqrt{1}/1*\\{( 0 ; 0); \n( 0 ; \\pm3); \n( \\pm4 ; 0); \n\\}'
        );
        done();
    });

    it("Should preserve negative coordinates", function(done){
        expect(
			new PointSet(
			1n,
			2n,
			[
				{ x:-8n, y:     -3n},
				{ x: 0n, y:      3n},
				{ x: 0n, y:     -3n},
				{ x:-8n, y:      3n},
			])
			.prepareDataForTeX()
        ).to.eql(
			'\\sqrt{1}/2*\\{( -8 ; \\pm3); \n( 0 ; \\pm3); \n\\}'
        );
        done();
    });


});
