﻿var mocha  = require('mocha')
  , assert = require('chai').assert
  , expect = require('chai').expect
  , PointSet = require('../PointSet.js')
  ;

describe("Testing isCrossNaive() function", function(){
    
	var ps = 
	[
		new PointSet(
		1n,
		1n,
		[
			{ x:-1n, y: 1n },
			{ x: 3n, y: 3n },
			{ x:-3n, y: 0n },
			{ x: 5n, y: 4n },
			{ x:-1n, y:-3n },
			{ x: 1n, y:-2n },
			{ x: 3n, y:-1n },
		]),
		new PointSet(
		1n,
		1n,
		[
			{ x: 0n, y: 0n },
			{ x: 2n, y: 0n },
			{ x: 2n, y: 2n },
		]),
		new PointSet(
		1n,
		1n,
		[
			{ x:-3n, y: 1n },
			{ x:-1n, y: 1n },
			{ x: 3n, y: 0n },
			{ x: 5n, y: 4n },
			{ x:-1n, y: 0n },
			{ x: 0n, y:-2n },
			{ x: 3n, y: 0n },
		]),
		new PointSet(
		1n,
		1n,
		[
			{ x:-3n, y: 1n },
			{ x:-1n, y: 8n },
			{ x: 3n, y: 7n },
			{ x: 5n, y: 4n },
			{ x:-1n, y: 1n },
			{ x: 1n, y:-2n },
			{ x: 3n, y: 2n },
		]),
		new PointSet(
		1n,
		1n,
		[
			{ x:-3n, y: 0n },
			{ x:-5n, y: 0n },
			{ x: 3n, y: 0n },
			{ x: 0n, y: 0n },
			{ x:-1n, y: 0n },
			{ x: 1n, y: 0n },
			{ x: 6n, y: 0n },
		]),
	];

    it("Should return true if count of different \"x\" of points not lying on X <= 1", function(done){
        expect(ps[1].isCrossNaive()).to.eql(true);
        expect(ps[4].isCrossNaive()).to.eql(true);
        done();
    });
	
    it("Should return false if count of different \"x\" of points not lying on X > 1", function(done){
        expect(ps[0].isCrossNaive()).to.eql(false);
        expect(ps[2].isCrossNaive()).to.eql(false);
        expect(ps[3].isCrossNaive()).to.eql(false);
        done();
    });
});

