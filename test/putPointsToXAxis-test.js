var mocha  = require('mocha')
  , assert = require('chai').assert
  , expect = require('chai').expect
  , PointSet = require('../PointSet.js')
  ;

describe("Testing putPointsOnXAxis(i1,i2) function", function(){
    
	var ps = 
	[
		new PointSet(
		1n,
		1n,
		[
			{ x: 2n, y: 6n},
			{ x: 6n, y: 3n},
			{ x: 6n, y: 6n},
		]),
		new PointSet(
		3n,
		1n,
		[
			{ x:-1n, y: 0n},
			{ x: 1n, y: 0n},
			{ x: 0n, y: 1n},
		]),
		new PointSet(
		3n,
		2n,
		[
			{ x: 56n, y:  0n},
			{ x:-56n, y:  0n},
			{ x: 14n, y:  0n},
			{ x:-34n, y:  0n},
			{ x:-10n, y: 24n},
			{ x:-21n, y: 35n},
			{ x: 35n, y:-21n},
		]),
		new PointSet(
		39n,
		8n,
		[
			{ x: 5040n, y:    0n},
			{ x:-5040n, y:    0n},
			{ x: 1911n, y:  315n},
			{ x: 2352n, y:    0n},
			{ x:  944n, y:    0n},
			{ x:  336n, y:    0n},
			{ x: 2940n, y: -420n},
			{ x: 2044n, y:  220n},
			{ x:  735n, y: 1155n},
		]),
		new PointSet(
		255255n,
		1n,
		[
			{x:        0n, y:    0n},
			{x:  8171520n, y:    0n},
			{x:  3482640n, y:-5040n},
			{x:  5202960n, y:-5040n},
			{x:  1384320n, y:    0n},
			{x:  6451200n, y:    0n},
			{x:  6289920n, y:    0n},
			{x:   514080n, y:    0n},
			{x:  2234400n, y:    0n},
			{x:  2395680n, y:    0n},
			{x:  7301280n, y:    0n},
			{x:  5150880n, y:    0n},
			{x:  3534720n, y:    0n},
			{x:  4384800n, y:    0n},
			{x:  4300800n, y:    0n},
			{x: 24612000n, y:    0n},
			{x:  8685600n, y:    0n},
			{x:  7666176n, y:    0n},
			{x:  5951008n, y:    0n},
			{x:  4656672n, y:    0n},
			{x:  4028928n, y:    0n},
			{x:  2734592n, y:    0n},
			{x:  1019424n, y:    0n},
			{x:-15926400n, y:    0n},
		]),
	];

    it("Should put points on X axis and don't destroy IPS", function(done){
		//for ps[0]
		ps[0] = ps[0].putPointsToXAxis(0,1);
        expect(ps[0].p[0]).to.eql({x:0n,y:0n});
        expect(ps[0].p[1].y).to.eql(0n);
        ps[0].reduce();
        expect(ps[0].isIPS()).to.eql(true);
		ps[0] = ps[0].putPointsToXAxis(0,2);
        expect(ps[0].p[0]).to.eql({x:0n,y:0n});
        expect(ps[0].p[2].y).to.eql(0n);
        ps[0].reduce();
        expect(ps[0].isIPS()).to.eql(true);
		ps[0] = ps[0].putPointsToXAxis(2,1);
        expect(ps[0].p[2]).to.eql({x:0n,y:0n});
        expect(ps[0].p[1].y).to.eql(0n);
        ps[0].reduce();
        expect(ps[0].isIPS()).to.eql(true);
        //for ps[1]
		ps[1] = ps[1].putPointsToXAxis(0,1);
        expect(ps[1].p[0]).to.eql({x:0n,y:0n});
        expect(ps[1].p[1].y).to.eql(0n);
        ps[1].reduce();
        expect(ps[1].isIPS()).to.eql(true);
		ps[1] = ps[1].putPointsToXAxis(0,2);
        expect(ps[1].p[0]).to.eql({x:0n,y:0n});
        expect(ps[1].p[2].y).to.eql(0n);
        ps[1].reduce();
        expect(ps[1].isIPS()).to.eql(true);
		ps[1] = ps[1].putPointsToXAxis(2,1);
        expect(ps[1].p[2]).to.eql({x:0n,y:0n});
        expect(ps[1].p[1].y).to.eql(0n);
        ps[1].reduce();
        expect(ps[1].isIPS()).to.eql(true);
        //for ps[2]
		ps[2] = ps[2].putPointsToXAxis(4,5);
        expect(ps[2].p[4]).to.eql({x:0n,y:0n});
        expect(ps[2].p[5].y).to.eql(0n);
        ps[2].reduce();
        expect(ps[2].isIPS()).to.eql(true);
		ps[2] = ps[2].putPointsToXAxis(0,1);
        expect(ps[2].p[0]).to.eql({x:0n,y:0n});
        expect(ps[2].p[1].y).to.eql(0n);
        ps[2].reduce();
        expect(ps[2].isIPS()).to.eql(true);
        //for ps[3]
		ps[3] = ps[3].putPointsToXAxis(0,1);
        expect(ps[3].p[0]).to.eql({x:0n,y:0n});
        expect(ps[3].p[1].y).to.eql(0n);
        ps[3].reduce();
        expect(ps[3].isIPS()).to.eql(true);
		ps[3] = ps[3].putPointsToXAxis(2,6);
        expect(ps[3].p[2]).to.eql({x:0n,y:0n});
        expect(ps[3].p[6].y).to.eql(0n);
        ps[3].reduce();
        expect(ps[3].isIPS()).to.eql(true);
        //for ps[4]
		ps[4] = ps[4].putPointsToXAxis(0,1);
        expect(ps[4].p[0]).to.eql({x:0n,y:0n});
        expect(ps[4].p[1].y).to.eql(0n);
        ps[4].reduce();
        expect(ps[4].isIPS()).to.eql(true);
		ps[4] = ps[4].putPointsToXAxis(2,3);
        expect(ps[4].p[2]).to.eql({x:0n,y:0n});
        expect(ps[4].p[3].y).to.eql(0n);
        ps[4].reduce();
        expect(ps[4].isIPS()).to.eql(true);
        done();
    });
    
});
