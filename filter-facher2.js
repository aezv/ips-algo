'use strict';

const PointSetGraphFactory = require('./PointSetGraphFactory');
const ls = require('ls');
const fs = require('fs');
const JSON = require('bigint-json-native');

for(var file of ls(process.argv[2] || 'aux/*_*_nofacher1.json')){
        if(fs.existsSync(file.full.replace('_nofacher1.json','_nofacher2.json'))){
        console.log('File exists: ' + file.full.replace('_nofacher1.json','_nofacher2.json'));
        continue;
    }
    var count = 1*file.name.split('_')[0];
    console.log(file, count);
    var psf = new PointSetGraphFactory();
    psf.readFromFileSync(file.full);
    console.log("Different chars before filtering", Object.keys(psf.graphs).length);

    delete psf.dontAppendCopiesInAdvance;
    delete psf.commonXpoints;

    try{
        for(var char in psf.graphs){
            let graph = psf.graphs[char];
            graph.noFacher = true;
            graph.noFacherDeeper = true;
            graph.filterWhilePossible(count);
            delete graph.noFacher;
            delete graph.noFacherDeeper;
        }
    }catch(e){
        console.error(e);
        console.log(file);
        return;
    }
    psf.filterAll(count);
    console.log("Different chars  after filtering", Object.keys(psf.graphs).length);
    var str = JSON.stringify(psf);
    console.log('Dump length:', str.length);
    fs.writeFileSync(file.full.replace('_nofacher1.json','_nofacher2.json'),str);
}