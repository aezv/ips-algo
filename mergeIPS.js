'use strict';
// Usage: node mergeIPS.js filename1.json filename2.json [stretchCoeff1 [stretchCoeff2]]

const fs = require('fs');
const JSON = require('bigint-json-native')({"BigNumber": BigInt, "noNew": true, "forceBig": true});
const PointSet = require('./PointSet');
const classifyAndWrite = require('./classifyAndWrite');
const uniteIPS = require('./uniteIPS');

let filename1 = process.argv[2];
let filename2 = process.argv[3];

let stretchCoeff1 = BigInt(process.argv[4] || 1n);
let stretchCoeff2 = BigInt(process.argv[5] || 1n);

let ps1 = new PointSet();
ps1.fromJSON(fs.readFileSync(filename1,'utf8'));

let ps2 = new PointSet();
ps2.fromJSON(fs.readFileSync(filename2,'utf8'));

// Stretching the PointSet-s

ps1.stretch(stretchCoeff1);
ps1.base /= stretchCoeff1;

ps1.reduce();

ps2.stretch(stretchCoeff2);
ps2.base /= stretchCoeff2;

ps2.reduce();

let ps = uniteIPS(ps1,ps2).ps;


console.log('Final length:', ps.p.length);
classifyAndWrite(ps,{symm:true});
