'use strict';

const isFullSquare = require('bigint-is-perfect-square');
const gcd = require('bigint-crypto-utils').gcd;
const abs = require('bigint-crypto-utils').abs;
const JSON = require('bigint-json-native')({"BigNumber": BigInt, "noNew": true, "forceBig": true});
const removeFromArray = require('./removeFromArray');
const sqrt = require('bigint-isqrt');

class PointSet {
    constructor(char, base, points) {
        this.char = char == 0 ? 1n : char; // Характеристика
        this.base = base ||  1n; // Удвоенная длина основания
        this.p = points  || []; // Собственно точки
    }
    isDistanceIntegral(p1,p2) {
        var s = (p1.x-p2.x)**2n + this.char*(p1.y-p2.y)**2n;
        if(!isFullSquare(s)){
            return false;
        }
        if(s%(this.base**2n)){
            return false;
        }
        if(!isFullSquare(s/(this.base**2n))){
            return false;
        }
        return true;
    }
    deepClone(){
        let ps = new PointSet(this.char,this.base,this.p.slice());
        for(let i = 0; i < this.p.length; i++){
            ps.p[i] = {x:this.p[i].x, y:this.p[i].y};
        }
        return ps;
    }

    countOutOfLineThrough(h,k){
        const x3 = this.p[k].x;
        const y3 = this.p[k].y;
        const A = x3-this.p[h].x;
        const B = y3-this.p[h].y;
        let out = 0;

        for(let i = 0; i < this.p.length; i++){
            if(A*(y3-this.p[i].y) !== B*(x3-this.p[i].x)){
                out++;
            }
        }

        return out;
    }

	isIPS() {
		for(let i=0;i<this.p.length; i++) {
			for(let j=i+1;j<this.p.length; j++) {
				if(!this.isDistanceIntegral(this.p[i],this.p[j])) {
					return false;
				}
			}
		}
		return true;
	}

    isFacher(){
        return (
            this.countOutOfLineThrough(this.p.length - 1, this.p.length - 2) === 1
        ||
            this.countOutOfLineThrough(                0,                 1) === 1
        )
    }

	inLineAndOutOfLine(h,k){
		const x3 = this.p[k].x;
        const y3 = this.p[k].y;
        const A = x3-this.p[h].x;
        const B = y3-this.p[h].y;
		var sortedPoints = [[],[]];

        for(let i = 0; i < this.p.length; i++){
            if(A*(y3-this.p[i].y) === B*(x3-this.p[i].x)){
                sortedPoints[0].push(i);//лежат на прямой
            }
			else
				sortedPoints[1].push(i);//не лежат на прямой
        }
		return sortedPoints;
	}

	isOnTwoLines(){
		var minOut = this.countOutOfLineThrough(0, 1); //здесь будет минимум точек вне прямой
		var minOutPoints = [0, 1]; //здесь будут номера точекс наименьшим minOut
		for (let i = 0; i < this.p.length - 1; i++)
			for (let j = i + 1; j < this.p.length; j++)
				if (this.countOutOfLineThrough(i, j) < minOut){
					minOut = this.countOutOfLineThrough(i, j);
					minOutPoints[0] = i;
					minOutPoints[1] = j;
				}
		var sortedPoints = this.inLineAndOutOfLine(minOutPoints[0],minOutPoints[1]);
		if (this.isFacher())
			return {
					isOnTwoLines: false,
					mostPointsOnLine: sortedPoints[0],
					isParallel: null,
					commonPoint: null
				};
		//sortedPoints[0] - точки прямой через minOutPoints
		//sortedPoints[1] - точки вне этой прямой
		//если прямая через любые две оставшиеся точки содержит их все, то система находится на двух прямых
		var l;
		var com, common = null;
		var testPS = this.inLineAndOutOfLine(sortedPoints[1][0], sortedPoints[1][1])[0];
		for (let i = 0; i < this.p.length; i++){
			l = false;
			com = 0;
			for (let j = 0; j < sortedPoints[0].length; j++)
				if (i === sortedPoints[0][j]){
					l = true;
					com++;
				}
			for (let j = 0; j < testPS.length; j++)
				if (i === testPS[j]){
					l = true;
					com++;
				}
			if (!l)
				return {
					isOnTwoLines: false,
					mostPointsOnLine: sortedPoints[0],
					isParallel: null,
					commonPoint: null
				};
			if (com == 2)
				common = i;
		}
		//console.log(sortedPoints[0].length);
		//console.log(sortedPoints[1].length);
		const A1 = this.p[sortedPoints[0][1]].y - this.p[sortedPoints[0][0]].y;
		const B1 = this.p[sortedPoints[0][1]].x - this.p[sortedPoints[0][0]].x;
		const A2 = this.p[sortedPoints[1][1]].y - this.p[sortedPoints[1][0]].y;
		const B2 = this.p[sortedPoints[1][1]].x - this.p[sortedPoints[1][0]].x;
		return {
			isOnTwoLines: true,
			indexesForLines: [sortedPoints[0], testPS],
			isParallel: (A1*B2 === A2*B1),
			commonPoint: common
		};
	}

	isInSemiGeneralPosition(){
		for (let i = 0; i < this.p.length - 1; i++)
			for (let j = i + 1; j < this.p.length; j++)
				if (this.inLineAndOutOfLine(i, j)[0].length > 2)
					return false;
		return true;
	}

    shift(x,y){
        for(let p of this.p){
            p.x += x;
            p.y += y;
        }
    }
    reduce(){
        var g = this.base;
        for(let p of this.p){
            //g = gcd(g, gcd(p.x, p.y));
            //console.log(g, p.x);
            if(p.x !== 0n){
                g = gcd(g, p.x);
            }
            //console.log(g, p.y);
            if(p.y !== 0n){
                g = gcd(g, p.y);
            }
            //console.log(g);
            if(g == 1){
                return;
            }
        }
        this.base /= g;
        for(let p of this.p){
            p.x /= g;
            p.y /= g;
        }
    }
    stretch(m){
        this.base *= m;
        for(let i in this.p){
            this.p[i]={
                x: this.p[i].x * m,
                y: this.p[i].y * m,
            }
        }
    }

    convertToBigInts(){
        this.char = BigInt(this.char);
        this.base = BigInt(this.base);
        for(let i in this.p){
            this.p[i]={
                x: BigInt(this.p[i].x),
                y: BigInt(this.p[i].y),
            }
        }
    }

    fromJSON(read){
        let json = JSON.parse(read);
        for(let prop in json){
            this[prop] = json[prop];
        }
    }

    guessCircleCenter(p1, p2, p3){
        this.convertToBigInts();
        var a = BigInt(p2.x - p1.x);
        var b = BigInt(p2.y - p1.y);
        var c = BigInt(p3.x - p1.x);
        var d = BigInt(p3.y - p1.y);

        //console.log(a, b, c, d);
        //console.log(p1,p2,p3);

        var e = a*(p1.x + p2.x) + b*(p1.y + p2.y) * this.char;
        var f = c*(p1.x + p3.x) + d*(p1.y + p3.y) * this.char;
        var g = 2n*(a*(p3.y - p2.y) - b*(p3.x - p2.x)) * this.char;// * sqrt (this.char)

        //если точки на одной прямой или совпадают
        if (g === 0n){
            return null;
        }
        //console.log(g, e, f);
        this.stretch(g);
        var center = {
            x: (d*e - b*f),// * this.char, // /g
            y: (a*f - c*e),// * this.char, // /g
        };
        //console.log('Center:', center);
        //console.log('Base:', this.base);
        return center;
    }

    diam(){
        let max = 0;
        for(let i = 0; i < this.p.length; i++) {
            for(let j = i + 1; j < this.p.length; j++) {
                let q = this.qasidistance(this.p[i],this.p[j]);
                if(max < q){
                    max = q;
                }
            }
        }
        return sqrt(max)/this.base;
    }

    distanceMatrix(){
        let matrix = [];
        for(let p1 of this.p){
            let row = [];
            for(let p2 of this.p){
                row.push(this.distance(p1,p2));
            }
            matrix.push(row);
        }
        return matrix;
    }

    distance(p1,p2){
        return sqrt(this.qasidistance(p1,p2))/this.base;
    }

    qasidistance(p1,p2){
        return (p1.x-p2.x)**2n + this.char*(p1.y-p2.y)**2n;
    }

    gcd(){
        var distances = this.distanceMatrix();
        var g = distances[0][1];
        for(let i=0;i<distances[0].length;i++){
            for(let j=i+1;j<distances[0].length;j++){
                g = gcd(g,distances[i][j]);
                if (g === 1n){
                    return g;
                }
            }
        }
        return g;
    }

    makePrime(){
        var ps = this.putPointsToXAxis(0,1);
        var dist_g = ps.gcd();
        ps.base *= dist_g;
        ps.reduce();
        //console.log(ps);
        return ps;
    }

    isCircular(){
        this.convertToBigInts();
        let center = this.guessCircleCenter(this.p[0], this.p[1], this.p[2]);
        if (!center){
            return false;
        }
        var radius = this.qasidistance(center, this.p[0]);
        for (let i = 3; i < this.p.length; i++){
            if(this.qasidistance(center, this.p[i]) != radius){
                return false;
            }
        }
        return true;
    }

	isAlmostOnCircle(){
		if (this.p.length < 4)
			return null;
		let ps1 = this.deepClone();
		if (ps1.isCircular())
			return null;
		let notOnCircle = -1;
		for (let i = 0; i < this.p.length; i++){
			let ps = this.deepClone();
			ps.p.splice(i, 1);
			if (ps.isCircular()){//для четырёх точек центр не ищет, тк любые 3 на окружности :(
				notOnCircle = i;
				break;
			}
		}
		if (notOnCircle === -1)
			return null;
		let index = [];
		for (let i = 0; i < this.p.length; i++)
			if (i !== notOnCircle)
				index.push(i);
		let ps = this.deepClone();
		let center = this.guessCircleCenter(this.p[index[0]], this.p[index[1]], this.p[index[2]]);
		if (center.x === this.p[notOnCircle].x && center.y === this.p[notOnCircle].y)
			return {
				isOnCircleWithCenter: true,
				initialPS: ps,
				indexOfCenter: notOnCircle,
				Center: ps.p[notOnCircle],
				PointsOnCircle: index
			};
		return {
				isAlmostCircle: true,
				initialPS: ps,
				notOnCircleIndex: notOnCircle,
				notOnCirclePoint: ps.p[notOnCircle],
				PointsOnCircle: index
			};
	}

	isFourOnCircle(){
		for (let i = 0; i < this.p.length - 3; i++)
			for (let j = i + 1; j < this.p.length - 2; j++)
				for (let k = j + 1; k < this.p.length - 1; k++)
					for (let l = k + 1; l < this.p.length; l++){
						let ps = new PointSet(
							this.char,
							this.base,
							[
								this.p[i],
								this.p[j],
								this.p[k],
								this.p[l]
							]
						);
						if (ps.isCircular())
							return true;
					}
		return false;
	}

	isCrossNaive(){
		let xs = {};
		for(let p of this.p){
			if(p.y != 0){
				xs[p.x] = 1;
			}
		}
		//console.log(this.p);
		//console.log(Object.keys(xs));
		return Object.keys(xs).length <= 1;
	}

    isCross(){
        if(this.isCrossNaive()){
            return true;
        }
        //console.log('Start check...');
        let ps = this.deepClone();
        ps.convertToBigInts();
        //console.log(ps.p);
        ps.shift(-ps.p[0].x, -ps.p[0].y);
        //console.log(ps.p);
        ps.rotateBig(1);
        //console.log(ps.p);
        if(ps.isCrossNaive()){
            return true;
        }
        ps = this.deepClone();
        ps.convertToBigInts();
        ps.shift(-ps.p[1].x, -ps.p[1].y);
        ps.rotateBig(2);
        //console.log(ps.p);
        if(ps.isCrossNaive()){
            return true;
        }
        ps = this.deepClone();
        ps.convertToBigInts();
        ps.shift(-ps.p[0].x, -ps.p[0].y);
        ps.rotateBig(2);
        //console.log(ps.p);
        if(ps.isCrossNaive()){
            return true;
        }
        return false;
    }

	rotateParallel(){
		let PSdata = this.isOnTwoLines();
		if (!PSdata.isParallel)
			return null;
		//console.log('Start check...');
        let ps = this.deepClone();
        ps.convertToBigInts();
        //console.log('Initial PS:\n',ps.p);
        ps.shift(-ps.p[PSdata.indexesForLines[0][0]].x, -ps.p[PSdata.indexesForLines[0][0]].y);
        //console.log(ps.p);
        ps.rotateBig(PSdata.indexesForLines[0][1]);
        //console.log('Rotation:\n',ps.p);
		return ps;
	}

	isSymmetrical(){
		for (let i = 0; i < this.p.length; i++){
			let l = true;
			if (this.p[i].x === 0n){
				continue;
			}
			else{
				for (let j = 0; j < this.p.length; j++){
					if ((i !== j) && (this.p[i].y === this.p[j].y) && (this.p[i].x === -this.p[j].x)){
						l = false;
						break;
					//console.log(this.p[i]);
					//console.log(this.p[j]);
					}
				}
			}
			if (l){
				//console.log(this.p[i]);
				return false;
			}
		}
		return true;
	}

	toSymmetrical(){
		if (this.isSymmetrical())
			return this;
		let ps = this.deepClone();
		for (let i = 0; i < this.p.length - 1; i++){
			for (let j = i + 1; j < this.p.length; j++){
				ps = this.deepClone();
				ps.convertToBigInts();
				ps.shift(-ps.p[i].x, -ps.p[i].y);
				ps.rotateBig(j);//поворот со сдвигом точки с инд i в начало координат
				if (ps.isSymmetrical())//если эта точка центр, система симметрична
					return ps;
				if (ps.p[j].x % 2n !== 0n){//если нет сдвигаем для симметрии по у
					ps.stretch(2n);
				}
				ps.shift(-ps.p[j].x/2n, 0n);
				if (ps.isSymmetrical())
					return ps;
			}
		}
		return null;
	}
	
	putPointsToXAxis(index1, index2) {
		let ps = this.deepClone();
		ps.convertToBigInts();
		ps.shift(-ps.p[index1].x, -ps.p[index1].y);
		ps.rotateBig(index2);//поворот со сдвигом точки с инд index1 в начало координат
		return ps;
	}
	
    rotate(index){
		if(this.p.length === 0){
			return;
		}
        let a = this.p[index].x;
        let b = this.p[index].y;
        let m = this.base;
        let q = this.char;
        if(!isFullSquare(a*a+b*b*q)){
            return;
        }

        this.base = m*sqrt(a*a+b*b*q);

        for(let p of this.p){
            let x = p.x, y = p.y;
            p.x = a*x + b*y*q;
            p.y = -b*x + a*y;
        }
    }

    rotateBig(index){
        let a = this.p[index].x;
        let b = this.p[index].y;
        let m = this.base;
        let q = this.char;

        let c = sqrt(a*a+b*b*q);

        this.base = m*c;

        for(let p of this.p){
            let x = p.x, y = p.y;
            p.x = a*x + b*y*q;
            p.y = -b*x + a*y;
        }
    }


    prepareDataForGnuplot(){
        var rez = '';
        for(var i=0; i<this.p.length; i++){
            rez += this.p[i].x/this.base + ' ' +
                // TODO: sgn
                (this.p[i].y < 0n ? -1 : 1) * Math.sqrt(Number(
                    this.p[i].y**2n*this.char/this.base**2n
                )) + '\n';
            //rez += this.p[i].x/this.base + ' ' + this.p[i].y*sqrt(this.char)/this.base + '\n';
        }
        return rez;
    }

    prepareDataForTextExport(){
        var rez = [this.p.length, this.char, this.base].join(' ') + '\n';
        for(var i=0; i<this.p.length; i++){
            rez += this.p[i].x + ' ' + this.p[i].y + '\n';
        }
        return rez;
    }

    prepareDataForTeX(){
		var rez = '\\sqrt{' + this.char + '}/' + this.base + '*';
		rez += '\\{';
		var p = this.p.slice();
		for(var i=0; i<p.length; i++){
			var foundNegative = false;
			for(var j=i+1; j<p.length; j++){
				if(p[i].x === -p[j].x && p[i].y === p[j].y){
					rez += '( \\pm'+abs(p[i].x) + ' ; ' + p[i].y + '); \n';
					foundNegative = true;
					p.splice(j,1);
					break;
				}
				if(p[i].x === p[j].x && p[i].y === -p[j].y){
					rez += '( '+p[i].x + ' ; \\pm' + abs(p[i].y) + '); \n';
					foundNegative = true;
					p.splice(j,1);
					break;
				}
			}
			if(!foundNegative){
				rez += '( '+p[i].x + ' ; ' + p[i].y + '); \n';
			}
		}
		rez += '\\}';
		return rez;
	}


}

module.exports = PointSet;
