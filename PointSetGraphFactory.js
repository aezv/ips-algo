const fs = require('fs');
const squarefree = require('./squarefree');
const isFullSquare = require('bigint-is-perfect-square');
const PointSetGraph = require('./PointSetGraph');
const PointSetDraft = require('./PointSetDraft');
const PointSet = require('./PointSet');
const removeFromArray = require('./removeFromArray');

const Performance = require('perf_hooks').performance;

const max = require('./bigint-max.js');

const JSON = require('bigint-json-native')({"BigNumber": BigInt, "noNew": true, "forceBig": true});

module.exports = class PointSetGraphFactory {

    constructor(){
        this.graphs={};
        this.drafts=[];
        this.ready=[];

        this.dontAppendCopiesInAdvance = false;
    }

    makePointSetGraphs(base, maxR){
        base = BigInt(base);
        maxR = BigInt(maxR);
        this.base = base * 2n;
        var m = base;
        this.commonXpoints = [];
        for(var i = m*m-2n*m; i > 0n; i-=2n*m){
            this.commonXpoints.push({x:i, y:0n});
        }
        for(var a = 1n; a <= maxR; a++){
            console.log(a);
            for(var b = max(a,m-a)+1n; b <= maxR; b++){
                var x = b**2n-a**2n;
                var yRoot = -(m**4n)+(2n*a**2n+2n*b**2n)*m**2n-b**4n+2n*a**2n*b**2n-a**4n;
                var y = squarefree(yRoot);
                var char = y[1], mult = y[0];
                this.ensureGraph(char);
                this.graphs[char].quad.push({x:x, y:mult});
            }
        }
        for(var a = (maxR+2n) / 2n; a <= maxR; a++){
            var yRoot = 4n*a**2n*m**2n-m**4n;
            var y = squarefree(yRoot);
            var char = y[1], mult = y[0];
            this.ensureGraph(char);
            this.graphs[char].duaY.push({x:0n, y:mult});
        }
    }
    appendCopies(char){
        this.graphs[char].duaX = this.commonXpoints.slice();
        if(this.base%2n === 0n){
            this.graphs[char].zero.push({x:0n,y:0n});
        }
    }
    ensureGraph(char){
        if(!this.graphs[char]){
            this.graphs[char] = new PointSetGraph();
            this.graphs[char].base = this.base;
            this.graphs[char].char = char;
            if(!this.dontAppendCopiesInAdvance){
                this.appendCopies(char);
            }
        }
    }

    filterAllPreliminary(friendsNeeded, minQuantityToFilter, maxQuantityToFilter){
        // We want to save memory.
        // So first of all we filter the graphs with less points
        let charsQuantity = Object.keys(this.graphs || {}).length;
        console.log('Filtering chars which have '+minQuantityToFilter+'..'+maxQuantityToFilter+' records')
        console.log('Total chars:', charsQuantity);
        let quadTrivials = 0;

        let totalAppendTime = 0;
        for(var char in this.graphs){
            var graph = this.graphs[char];

            var records = graph.getQuantityOfRecords();
            if (records > maxQuantityToFilter || records < minQuantityToFilter){
                continue;
            }

            // TODO: first of all, just copy links to common points
            // and try to filter quad points and duaY points

            var perf = Performance.now()
            if(this.dontAppendCopiesInAdvance){
                this.appendCopies(char);
            }
            totalAppendTime += (Performance.now()-perf);

            //perf = Performance.now()
            graph.filterQuad(friendsNeeded);
            graph.filterDuaY(friendsNeeded);
            //console.log('p',Performance.now()-perf);
            if(!graph.isTrivial()){
                perf = Performance.now()
                graph.filterZero(friendsNeeded);
                graph.filterDuaX(friendsNeeded);
                graph.filterWhilePossible(friendsNeeded);
                console.log(Performance.now()-perf);
            } else {
                quadTrivials++;
            }
            if(graph.isTrivial()){
                delete this.graphs[char];
                charsQuantity--;
                if(charsQuantity % 100 === 0){
                    console.log('Chars left:', charsQuantity);
                }
            }
        }
        console.log('quadTrivials:', quadTrivials);
        console.log('totalAppendTime:',totalAppendTime);
    }


    filterAll(friendsNeeded){
        this.filterAllPreliminary(friendsNeeded, 0, 1);
        this.filterAllPreliminary(friendsNeeded, 2, 2);
        this.filterAllPreliminary(friendsNeeded, 3, 3);
        this.filterAllPreliminary(friendsNeeded, 4, 4);
        this.filterAllPreliminary(friendsNeeded, 5, 5);
        this.filterAllPreliminary(friendsNeeded, 6, 10);
        this.filterAllPreliminary(friendsNeeded, 11, Infinity);


        // The copies are on their place anyway
        this.dontAppendCopiesInAdvance = false;


		this.drafts.sort((a,b)=>(b.p.length - a.p.length));
		console.log('sorted');

		//this.drafts.map(d=>console.log(d.p.length));

		//process.exit();

		while(this.drafts.length){
			let draft = this.drafts.pop();

            draft.filter(friendsNeeded - draft.clique.length);
            if (
                (draft.clique.length + draft.p.length < friendsNeeded)
            ||
                (this.noFacher && draft.isFacherNaive())
            ){
                continue;
            }

            draft.enlargeClique();
            if(!draft.p.length){
				let obtainedSet = new PointSet(
                    draft.char,
                    draft.base,
                    draft.clique
                );

				if(this.noFacher && obtainedSet.isFacher()){
					// Just ignore it!
					continue;
				}

                this.ready.push();
                continue;
            }
            if(this.forkDrafts){
                this.drafts.push(draft.fork());
                this.drafts.push(draft);
            }

            /*if(this.drafts.length > 2980){
				continue;
			}*/

			if(this.drafts.length%10 === 0 && this.drafts[this.drafts.length - 1].p.length > 7){
				console.log(this.drafts.length, this.ready.length);
				console.log(this.drafts[this.drafts.length - 1].p.length);
				console.log('************');
			}
		}

        if(this.noFacher){
            for(let i = 0; i < this.ready.length; i++){
                if(this.ready[i].isFacher()){
                    removeFromArray(this.ready, i);
                    i--;
                }
            }
        }
    }

    filterQuadTrivialsNoFacher(){
        let m = this.base;
        let m2 = m ** 2n;
        let quadTrivials = 0;
        for(var char in this.graphs){
            var graph = this.graphs[char];
            if(graph.duaY.length > 0 || graph.quad.length > 1){
                // Not a quad-trivial
                continue;
            }
            let x = graph.quad[0].x;
            let y = graph.quad[0].y;
            let q = graph.char;
            if((x << 1n) % m == 0n){
                // There is possibly 2 points on the line parallel to Ox
                continue;
            }
            let diagonal = 4n*(x**2n + q * (y**2n));
            if(diagonal%m2 || !isFullSquare(diagonal/m2)){
                delete this.graphs[char];
                quadTrivials++;
                if(quadTrivials%2000==0){
                    console.log('quadTrivials deleted:', quadTrivials);
                }
                continue;
            }
        }
        console.log('quadTrivials deleted:', quadTrivials);
    }

    readFromFileSync(filename){
        var parsed = JSON.parse(fs.readFileSync(filename,'utf8'));
        Object.assign(this, parsed);
        for(let char in this.graphs){
            this.graphs[char] = Object.assign(new PointSetGraph(), this.graphs[char]);
        }
        for(let  num in this.drafts){
            this.drafts[ num] = Object.assign(new PointSetDraft(), this.drafts[ num]);
        }
        for(let  num in this.ready ){
            this.ready [ num] = Object.assign(new PointSet     (), this.ready [ num]);
        }
    }
    separateOneDraft(char){
        let g = this.graphs[char];
        if(g.quad.length){
            let point = g.quad[g.quad.length - 1];
            let friends = g.getFriends(point);
            g.quad.length--;
            let d = new PointSetDraft(
                g.char,
                g.base,
                friends,
                [
                    {x: ((this.base/2n)**2n), y:0n},
                    {x:-((this.base/2n)**2n), y:0n},
                ]
            );
            this.drafts.push(d);
        }
    }
}
