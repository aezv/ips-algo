'use strict';

// Usage:
// nodejs generate-origins.js X Y
// Generates origins for diameters from X to Y (including X and Y)

const PointSetGraphFactory = require('./PointSetGraphFactory');
const fs = require('fs');


for(let j = 6; j <= 6; j++){

    var t2 = Date.now();
    var psf = new PointSetGraphFactory();
    psf.dontAppendCopiesInAdvance = true;
    let m = 3;
    psf.makePointSetGraphs(m,8);
    console.log("Generating points, ms:", Date.now()-t2);


    console.log("Different chars before filtering", Object.keys(psf.graphs).length);
    t2 = Date.now();
    psf.filterAll(j);
    console.log("Different chars ", Object.keys(psf.graphs).length);
    //var str = JSON.stringify(psf);
    //console.log('Dump length:', str.length);
    console.log("Filtering points, ms:", Date.now()-t2);
    //fs.writeFileSync('aux/'+j+'_'+1200+'_origin-minimeter-2.json',str);

    delete psf.dontAppendCopiesInAdvance;
    //delete psf.commonXpoints;

    try{
        //psf.noFacher = true;
        psf.forkDrafts = true;
        for(var char in psf.graphs){
            let graph = psf.graphs[char];
            //graph.noFacher = true;
            //graph.noFacherDeeper = true;
            while(graph.quad.length){
                psf.separateOneDraft(char);
                //console.log('Quad points left for char ' + char + ' : ' + graph.quad.length);
            }
            delete psf.graphs[char];
        }
        delete psf.graphs;
    }catch(e){
        console.error(e);
        //console.log(file);
        return;
    }

    console.log('!');
    psf.filterAll(j);
    var str = JSON.stringify(psf);
    console.log('Dump length:', str.length);
    console.log('Ready sets:', psf.ready.length);
    console.log('Ready sets length in dump:', JSON.stringify(psf.ready).length);
    console.log('Drafts:', psf.drafts.length);
    fs.writeFileSync('aux/'+j+'_minimeter-'+m+'.json', str);
}

/*
var t2 = Date.now();
var psf = new PointSetGraphFactory();
psf.dontAppendCopiesInAdvance = true;
var d = 500;
psf.makePointSetGraphs(d,d);
console.log("Different chars before filtering", Object.keys(psf.graphs).length);
console.log("Generating points, ms:", Date.now()-t2);
psf.filterAll(8);
console.log("Different chars after filtering", Object.keys(psf.graphs).length);
*/
/*
var psf = new PointSetGraphFactory();
psf.readFromFileSync('aux/7_19_origin.json');
console.log("Different chars before filtering", Object.keys(psf.graphs).length);
psf.filterAll(8);
console.log("Different chars after filtering", Object.keys(psf.graphs).length);
*/