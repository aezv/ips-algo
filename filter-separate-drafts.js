'use strict';

const PointSetGraphFactory = require('./PointSetGraphFactory');
const ls = require('ls');
const fs = require('fs');
const JSON = require('bigint-json-native');

for(var file of ls(process.argv[2] || 'aux/23_1110_*_nofacher2.json')){
    var newFilename = file.full.replace('_nofacher2.json','_sep1.json');
    if(fs.existsSync(newFilename)){
        console.log('File exists: ' + newFilename);
        continue;
    }
    var count = 1*file.name.split('_')[0];
    console.log(file, count);
    var psf = new PointSetGraphFactory();
    psf.readFromFileSync(file.full);
    console.log("Different chars before filtering", Object.keys(psf.graphs).length);


    delete psf.dontAppendCopiesInAdvance;
    delete psf.commonXpoints;

    try{
        psf.noFacher = true;
        psf.forkDrafts = true;
        for(var char in psf.graphs){
			console.log("Processing char: "+char);
            let graph = psf.graphs[char];
            graph.noFacher = true;
            graph.noFacherDeeper = true;
            while(graph.quad.length){
                psf.separateOneDraft(char);
            }
            delete psf.graphs[char];
        }
        delete psf.graphs;
    }catch(e){
        console.error(e);
        console.log(file);
        return;
    }

    console.log('Drafts:', psf.drafts.length);


    psf.filterAll(count);
    var str = JSON.stringify(psf);
    console.log('Dump length:', str.length);
    console.log('Ready sets:', psf.ready.length);
    console.log('Ready sets length in dump:', JSON.stringify(psf.ready).length);
    console.log('Drafts:', psf.drafts.length);
    fs.writeFileSync(newFilename, str);
}
