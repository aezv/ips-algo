'use strict';

const PointSetGraphFactory = require('./PointSetGraphFactory');
const ls = require('ls');
const fs = require('fs');
const JSON = require('bigint-json-native');
const classifyAndWrite = require('./classifyAndWrite');

for(var file of ls(process.argv[2] || 'aux/8_*_sep1.json')){
	var count = 1*file.name.split('_')[0];
	console.log(file, count);
	var psf = new PointSetGraphFactory();
	psf.readFromFileSync(file.full);

	try{
		for(let result of psf.ready){
			classifyAndWrite(result);
		}


	}catch(e){
		console.error(e);
		console.log(file);
		return;
	}
}
