const ReadyPrimes = require('ready-primes-extended').ReadyPrimes;

/*
ReadyPrimes.primes(100000001).then( (result) => {
    console.log(result);
    console.log(result.length);
    console.log(result.slice(-10));
    // output: [ 2, 3, 5, 7 ]
    var fs = require('fs');
    var additional = fs.readFileSync('primes1.txt').split(/\s/g).map(a=>1*a);
    var maxPrime = result[result.length-1];


    fs.writeFileSync('primes.json',JSON.stringify(result))
});
*/

var fs = require('fs');
var result = fs.readFileSync('primes1.txt','utf8').split(/\s/g).map(a=>1*a);


fs.writeFileSync('primes.json',JSON.stringify(result))
