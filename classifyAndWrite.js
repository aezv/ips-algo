'use strict';

const drawGnuplot = require('./drawGnuplot');
const ls = require('ls');
const fs = require('fs');
const crypto = require('crypto');
const JSON = require('bigint-json-native');
const mkdirp = require('mkdirp');


module.exports = function(result, classesToIgnore){
    let clone = result.deepClone();
    clone.reduce();
	classesToIgnore = classesToIgnore || {};//нужно установить значение класса "true", чтобы игнорировать
	if (!classesToIgnore.circular && clone.isCircular()) {
		console.log('Circular!');
		return;
	}
    clone = result.deepClone();
	if (!classesToIgnore.cross && clone.isCross()) {
		console.log('Cross!');
		return;
	}
    let suffix = 'other';
	if(!classesToIgnore.pyramid || !classesToIgnore.x || !classesToIgnore.rails){
		let PSData = clone.isOnTwoLines();
		if(!PSData.isParallel && PSData.isOnTwoLines){
			if(!classesToIgnore.pyramid){
				if(clone.toSymmetrical()){
					suffix = 'pyramid';
				} else if (!classesToIgnore.x) {
					suffix = 'X';
				}
			}
		} else if(!classesToIgnore.rails && PSData.isParallel) {
			suffix = 'rails_'+PSData.indexesForLines[1].length;
			result = clone.rotateParallel().deepClone();
		}
	}
	if (suffix == 'other' && (!classesToIgnore.centered_circle || !classesToIgnore.almost_circle)){
        clone = result.deepClone();
        let isAOCircle = clone.isAlmostOnCircle();
		if(isAOCircle && isAOCircle.isOnCircleWithCenter && !classesToIgnore.centered_circle){
			suffix = 'centered_circle';
		} else if (isAOCircle && isAOCircle.isAlmostCircle && !classesToIgnore.almost_circle){
			suffix = 'almost_circle';
		}
	}
	if (suffix == 'other' && !classesToIgnore.symm) {
		clone = result.deepClone();
		let symm = clone.toSymmetrical();
		if(symm){
			result = symm;
			suffix = 'symm';
		}
	}

    result.reduce();
    let json = JSON.stringify(result);
    let hash = crypto.createHash('md5').update(json).digest("hex");
    let text = result.prepareDataForGnuplot();
    mkdirp.sync('found/'+suffix);
    let filename = 'found/'+suffix+'/'+[
        result.p.length,
        result.diam(),
        result.char,
        hash,
        suffix,
    ].join('_');

    fs.writeFileSync(filename+'.json', json);
    fs.writeFileSync(filename+'.txt', text);
    try{
        drawGnuplot(filename);
    } catch(e){
        console.log('Gnuplot error:');
        console.log(e);
    }
    fs.writeFileSync(filename+'.ips.txt', result.prepareDataForTextExport());
    fs.writeFileSync(filename+'.ips.tex', result.prepareDataForTeX());
    fs.writeFileSync(filename+'.dist.txt', result.distanceMatrix().map(r=>r.join('\t\t')).join('\n'));

}
