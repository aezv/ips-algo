const PointSetGraph = require('./PointSetGraph');

var psg = new PointSetGraph();
psg.char = 1;
psg.base = 2;

psg.zero = [
    {x:0, y:0},
];

psg.duaX = [
    {x:8, y:0},
];

psg.duaY = [
    {x:0, y:6},
];

psg.quad = [
    {x:8, y:6},
];

console.log(psg);

psg.filterFacherPointsDeeper();

console.log(psg);

console.log(psg.getFriends({x:0, y:6}));

psg.filterCrossPoints();
console.log(psg);


psg.quad=[];
psg.filterCrossPoints();
console.log(psg);
