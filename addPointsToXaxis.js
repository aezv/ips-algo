'use strict';
// Usage: node addPointsToXaxis.js filename.json steps stretchCoeff

const fs = require('fs');
const JSON = require('bigint-json-native')({"BigNumber": BigInt, "noNew": true, "forceBig": true});
const PointSet = require('./PointSet');
const classifyAndWrite = require('./classifyAndWrite');

//console.log(process.argv);
let filename = process.argv[2];
let steps = BigInt(process.argv[3]);
let stretchCoeff = BigInt(process.argv[4]);

let ps = new PointSet();
ps.fromJSON(fs.readFileSync(filename,'utf8'));

// Stretching the PointSet

ps.stretch(stretchCoeff);
ps.base /= stretchCoeff;

ps.reduce();

//console.log(ps);

// Let us found the first point on X axis
let shift = null;
for(let point of ps.p){
    if(point.y === 0n){
        shift = point.x;
        break;
    }
}

if(shift === null){
    console.log('No points on X axis, is the data correct?');
}

let stepsDone = 0n;
for(let x = shift - ps.base * steps; x <= shift + ps.base * steps; x += ps.base){
    // TODO: fastenate
    let candidate = {x: x, y: 0n};
    let good = true;
    for(let point of ps.p){
        if((point.x === candidate.x) || (point.y && !ps.isDistanceIntegral(point,candidate)) ){
            good = false;
            break;
        }
    }
    if(good){
        ps.p.push(candidate);
        console.log(ps.p.length);
		classifyAndWrite(ps);
    }
    stepsDone++;
    if(stepsDone % 100000n === 0n){
		console.log(stepsDone + ' of ' + 2n*steps);
	}
}

console.log('Final length:', ps.p.length);
classifyAndWrite(ps,{symm:true});
