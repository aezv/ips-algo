var PointSet = require('./PointSet.js');
const classifyAndWrite = require('./classifyAndWrite');
const isFullSquare = require('bigint-is-perfect-square');
var uniteIPS = function (ps1, ps2) {
	if (ps1.char != ps2.char) {
		console.log("Unable to unite: distinct characteristics");
		return;
	}
	if (ps1.base != ps2.base) {
		let b1=ps1.base, b2=ps2.base;
		ps1.stretch(b2);
		ps2.stretch(b1);
	}
	var ps = new PointSet(ps1.char, ps1.base);
	for(let i=0; i<ps1.p.length; i++) {
		ps.p.push({x: ps1.p[i].x, y: ps1.p[i].y});
	}
	var extraPS = new PointSet(ps1.char, ps1.base);
	for(let i=0; i<ps2.p.length; i++) {
		let e = true;
		for (let j=0;j<ps.p.length;j++){
			if(ps.p[j].x == ps2.p[i].x && ps.p[j].y == ps2.p[i].y){
				e = false;
				break;
			}
		}
		if(e){
			ps.p.push({x: ps2.p[i].x, y: ps2.p[i].y});
			for(k=0;k<ps.p.length-1;k++){
				var s = (ps.p[ps.p.length-1].x-ps.p[k].x)**2n + ps.char*(ps.p[ps.p.length-1].y-ps.p[k].y)**2n;
				if(!isFullSquare(s)){
					console.log("not fullsquare ", ps.p[ps.p.length-1],ps.p[k], s);
					extraPS.p.push(ps.p.splice(-1)[0]);
					break;
				}
				if(s%(ps.base**2n)){
					console.log("fullsquare but not integral-1 ", ps.p[ps.p.length-1],ps.p[k], s);
					extraPS.p.push(ps.p.splice(-1)[0]);
					break;
				}
				if(!isFullSquare(s/(ps.base**2n))){
					console.log("fullsquare but not integral-2 ", ps.p[ps.p.length-1],ps.p[k], s);
					extraPS.p.push(ps.p.splice(-1)[0]);
					break;
				}
				//console.log("integral ", ps.p[ps.p.length-1],ps.p[k], s);
			}
			/*if (!ps.isIPS()){
				extraPS.p.push(ps.p.splice(-1)[0]);
			}*/
		}
	}
	return {ps: ps, extraPS: extraPS};
}

/*var ps = [
	new PointSet(
		143n,
		5040n,
		[
			{ x: 6350400n, y:      0n},
			{ x:-6350400n, y:      0n},
			{ x:  573300n, y: 220500n},
			{ x:-2797740n, y: 247860n},
			{ x: 3457440n, y:-282240n},
			{ x:  446400n, y:-576000n},
			{ x:  615600n, y: 486000n},
			{ x: 3175200n, y:      0n},
		]),
	new PointSet(
		143n,
		5040n,
		[
			{ x: 6350400n, y:      0n},
			{ x:-6350400n, y:      0n},
			{ x:  573300n, y: 220500n},
			{ x: 2881200n, y: 294000n},
			{ x:  446400n, y:-576000n},
			{ x: 4084800n, y: 192000n},
			{ x:  615600n, y: 486000n},
			{ x: 3175200n, y:      0n},
		]),
	new PointSet(
		143n,
		5040n,
		[
			{x: 6350400n,y:      0n},
			{x:-6350400n,y:      0n},
			{x:  573300n,y: 220500n},
			{x:-2797740n,y: 247860n},
			{x:  615600n,y: 486000n},
			{x: 4084800n,y: 192000n},
			{x:  446400n,y:-576000n},
			{x: 3175200n,y:      0n},
		]),
	new PointSet(
		143n,
		5040n,
		[
			{x: 6350400n,y:      0n},
			{x:-6350400n,y:      0n},
			{x:  573300n,y: 220500n},
			{x: 2881200n,y: 294000n},
			{x: 3457440n,y:-282240n},
			{x:  446400n,y:-576000n},
			{x:  615600n,y: 486000n},
			{x: 3175200n,y:      0n},
		]),
];
var ps = [
	new PointSet(
		154n,
		1n,
		[
			{x:          0n,y:        0n},
			{x: 20044577280n,y: 0n},
			{x: 3656387280n,y: -437018400n},
			{x: 17568139680n,y: 0n},
			{x: 2476437600n,y: 0n},
			{x: 16570281000n,y: 0n},
			{x: 3474296280n,y: 0n},
			{x: 16206099000n,y: 0n},
			{x: 3838478280n,y: 0n},
			{x: 14487159960n,y: 0n},
			{x: 5557417320n,y: 0n},
			{x: 14217665280n,y: 0n},
			{x: 5826912000n,y: 0n},
			{x: 10750652640n,y: 0n},
			{x: 9293924640n,y: 0n},
			{x: 16388190000n,y: -437018400n},
			{x: -25837915168n,y: 0n},
			{x: -24637952820n,y: 0n},
			{x: -13962737880n,y: 0n},
			{x: -10104489720n,y: 0n},
			{x: -4904317600n,y: 0n},
			{x: -3437878080n,y: 0n},
			{x: -392102620n,y: 0n},
			{x: 7827485120n,y: 0n},
			{x: 9632995424n,y: 0n},
			{x: 10411581856n,y: 0n},
			{x: 12217092160n,y: 0n},
			{x: 20436679900n,y: 0n},
			{x: 23482455360n,y: 0n},
			{x: 24948894880n,y: 0n},
			{x: 30149067000n,y: 0n},
			{x: 34007315160n,y: 0n},
			{x: 7350103215n,y: 0n},
			{x: 12694474065n,y: 0n},
			{x: 44682530100n,y: 0n},
			{x: 45882492448n,y: 0n},
			{x: 164442180n,y: 0n},
			{x: 8729162400n,y: 0n},
			{x: 11315414880n,y: 0n},
			{x: 19880135100n,y: 0n},
			{x: 6620227068n,y: 0n},
			{x: 13424350212n,y: 0n},
		]),
	new PointSet(
		154n,
		1n,
		[
			{x:          0n,y:        0n},
			{x: 20044577280n,y: 0n},
			{x: 3656387280n,y: -437018400n},
			{x: 17568139680n,y: 0n},
			{x: 2476437600n,y: 0n},
			{x: 16570281000n,y: 0n},
			{x: 3474296280n,y: 0n},
			{x: 16206099000n,y: 0n},
			{x: 3838478280n,y: 0n},
			{x: 14487159960n,y: 0n},
			{x: 5557417320n,y: 0n},
			{x: 14217665280n,y: 0n},
			{x: 5826912000n,y: 0n},
			{x: 10750652640n,y: 0n},
			{x: 9293924640n,y: 0n},
			{x: 16388190000n,y: -437018400n},
			{x: -25837915168n,y: 0n},
			{x: -24637952820n,y: 0n},
			{x: -13962737880n,y: 0n},
			{x: -10104489720n,y: 0n},
			{x: -4904317600n,y: 0n},
			{x: -3437878080n,y: 0n},
			{x: -392102620n,y: 0n},
			{x: 7827485120n,y: 0n},
			{x: 9632995424n,y: 0n},
			{x: 10411581856n,y: 0n},
			{x: 12217092160n,y: 0n},
			{x: 20436679900n,y: 0n},
			{x: 23482455360n,y: 0n},
			{x: 24948894880n,y: 0n},
			{x: 30149067000n,y: 0n},
			{x: 34007315160n,y: 0n},
			{x: 7350103215n,y: 0n},
			{x: 12694474065n,y: 0n},
			{x: 44682530100n,y: 0n},
			{x: 45882492448n,y: 0n},
			{x: 164442180n,y: 0n},
			{x: 8729162400n,y: 0n},
			{x: 11315414880n,y: 0n},
			{x: 19880135100n,y: 0n},
			{x: -7677602400n,y: 0n},
			{x: -4386258240n,y: 0n},
			{x: 7014764526n,y: 0n},
			{x: 13029812754n,y: 0n},
			{x: 24430835520n,y: 0n},
			{x: 27722179680n,y: 0n},
		]),
];
ps[0].stretch(29n);
ps[1].stretch(23n);
for (let i=0; i< ps.length; i++){
	for (let j=0; j<ps.length; j++){
		if (i!=j){
			var res = uniteIPS(ps[i],ps[j]);
			if(res.ps.p.length > Math.min(ps[i].p.length, ps[j].p.length)) {
				console.log(res);
				classifyAndWrite(res.ps);
			}
			else {
				console.log(i,j,res.ps.p,res.extraPS.p);
			}
		}
	}
}
for (let i=0; i<ps.length; i++) {
	ps[i] = ps[i].putPointsToXAxis(7, 2);
	classifyAndWrite(ps[i]);
}*/

module.exports = uniteIPS;
